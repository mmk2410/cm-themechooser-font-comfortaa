Theme Chooser Font Comfortaa
==============================

Description
------------------------------

This is the open source Comfortaa font by aajohan (aka Johan Aakerlund), packaged for the Cyanogenmod Theme Engine. All the credits go to aajohan. This font has no italic types.

The package name is de.marcelkapfer.cyngn.theme.comfortaa

The App icon is by Cyanogenmod.

Licenses
------------------------------

Icon License: http://www.apache.org/licenses/LICENSE-2.0.html <br>
Font License: http://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web

Contributing
------------------------------

1. Fork it
2. Create a feature branch with a meaningful name (`git checkout -b my-new-feature`)
3. Add yourself to the CONTRIBUTORS file
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to your branch (`git push origin my-new-feature`)
6. Create a new pull request
